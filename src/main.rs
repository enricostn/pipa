use std::io::{self, BufRead, Write};

struct Item {
    trigger: String,
    value: String
}

impl Item {
    fn new(trigger: String, value: String) -> Item {
        Item { trigger: trigger, value: value }
    }
}

fn trigger(index: &usize) -> String {
    "o".to_string()
}

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().enumerate().map(|t| Item::new(trigger(&t.0), t.1.unwrap()));
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    match handle.write(b"Hola")
}
